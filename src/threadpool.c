/**
 *  @file threadpool.c
 *  @author Ryan Greer <ryan.greer@outlook.com>
 *  @date 22 October 2019
 *
 *  @brief Functions for the Threadpool shared library.
 */

/* Standard Library Headers */
#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/* Threadpool Library Headers */
#include "threadpool.h"

/* Private variables */
static int threadpool_task_limit;

/* Private function prototypes */
static int init_threads(threadpool_t* tpp, int thread_count);
static int free_pool(threadpool_t* tpp);
static void* start_thread(threadpool_t* tpp);

/* Public functions */

/**
 *  @brief Create a threadpool.
 */
threadpool_t* threadpool_create(const int thread_count, const int task_limit)
{
    threadpool_t* tpp = NULL;
    threadpool_task_limit = task_limit;

    if (!((thread_count > 0) && (task_limit >= thread_count)))
    {
        errno = EINVAL;
        return NULL;
    }

    if (NULL == (tpp = (threadpool_t*) calloc(1, sizeof(threadpool_t))))
    {
        /* GCOVR_EXCL_START */
        return NULL;
        /* GCOVR_EXCL_STOP */
    }

    if (NULL == (tpp->threads = (pthread_t**) calloc(1, sizeof(pthread_t*) * thread_count)))
    {
        /* GCOVR_EXCL_START */
        threadpool_shutdown(tpp);
        return NULL;
        /* GCOVR_EXCL_STOP */
    }

    tpp->thread_count = 0;

    tpp->task_head = NULL;
    tpp->task_count = 0;

    pthread_mutex_init(&(tpp->lock), NULL);
    pthread_cond_init(&(tpp->wake_up), NULL);

    if (init_threads(tpp, thread_count) != thread_count)
    {
        /* GCOVR_EXCL_START (now way to simulate this failure) */
        threadpool_shutdown(tpp);
        return NULL;
        /* GCOVR_EXCL_STOP */
    }

    /* Set the threadpool status to THREADPOOL_READY */
    tpp->state = TP_READY;

    return tpp;
}

/**
 *  @brief Add a task to the threadpool's task list
 */
int threadpool_schedule(threadpool_t* tpp, void (*func_ptr)(void*), void* argument)
{
    if (!(func_ptr && argument && tpp))
    {
        return ENULLPTR;
    }

    if (tpp->task_count >= threadpool_task_limit)
    {
        return EBUSY;
    }

    struct tp_task_s* new_task_p;
    struct tp_task_s* task_tail;

    if (NULL == (new_task_p = (struct tp_task_s*) calloc(1, sizeof(struct tp_task_s))))
    {
        /* GCOVR_EXCL_START */
        return TP_FAIL;
        /* GCOVR_EXCL_STOP */
    }

    new_task_p->func_ptr = func_ptr;
    new_task_p->argument = argument;
    new_task_p->next_task = NULL;

    pthread_mutex_lock(&(tpp->lock));

    /*
     *  If the threadpool's task_head is NULL, set it to the new task
     *  Otherwise, find the last task and set its next_task to the new task
    */
    if (NULL == tpp->task_head)
    {
        tpp->task_head = new_task_p;
    }
    else
    {
        task_tail = tpp->task_head;

        while(task_tail->next_task != NULL)
        {
            task_tail = task_tail->next_task;
        }

        task_tail->next_task = new_task_p;
    }

    tpp->task_count++;

    if (pthread_cond_broadcast(&(tpp->wake_up)) != 0)
    {
        /* GCOVR_EXCL_START */
        return ETHREADLCK;
        /* GCOVR_EXCL_STOP */
    }

    pthread_mutex_unlock(&(tpp->lock));

    return TP_SUCCESS;
}

/**
 *  @brief Destroy and free a threadpool
 */
int threadpool_shutdown(threadpool_t* tpp)
{
    if (NULL == tpp)
    {
        return ENULLPTR;
    }

    tpp->state = TP_DOWN;

    pthread_mutex_lock(&(tpp->lock));

    if (pthread_cond_broadcast(&(tpp->wake_up)) != 0)
    {
        /* GCOVR_EXCL_START */
        return ETHREADLCK;
        /* GCOVR_EXCL_STOP */
    }

    pthread_mutex_unlock(&(tpp->lock));

    if (tpp->thread_count > 0)
    {
        for (int thread = 0; thread < tpp->thread_count; thread++)
        {
            void* thread_res;
            pthread_join(*(tpp->threads[thread]), &thread_res);
        }
    }

    pthread_mutex_lock(&(tpp->lock));
    pthread_mutex_destroy(&(tpp->lock));
    pthread_cond_destroy(&(tpp->wake_up));

    free_pool(tpp);

    return TP_SUCCESS;
}

/* Private functions */

/**
 *  @fn static int init_threads(threadpool_t* tpp, int count) (function)
 *
 *  Initialize the threads in the threadpool pointed to by threadpool_p.
 *
 *  @param tpp Pointer to a threadpool.
 *  @param count Integer representing the desired number of threads.
 *
 *  @return Integer indicating the number of threads successfully initialized.
 */
static int init_threads(threadpool_t* tpp, int count)
{
    pthread_mutex_lock(&(tpp->lock));

    for (int thread_idx = 0; thread_idx < count; thread_idx++)
    {
        pthread_t* pthread_p;

        if (NULL == (pthread_p = (pthread_t*) calloc(1, sizeof(pthread_t))))
        {
            /* GCOVR_EXCL_START */
            break;
            /* GCOVR_EXCL_STOP */
        }

        if (pthread_create(pthread_p, NULL, (void*) start_thread, tpp) != 0)
        {
            /* GCOVR_EXCL_START */
            break;
            /* GCOVR_EXCL_STOP */
        }

        tpp->threads[thread_idx] = pthread_p;
        tpp->thread_count++;
    }

    pthread_mutex_unlock(&(tpp->lock));

    return tpp->thread_count;
}

/**
 *  @fn static int free_pool(threadpool_t* tpp) (function)
 *
 *  Free the tasks and threads of a threadpool from memory.
 *
 *  @param tpp Pointer to a threadpool
 *
 *  @return Integer indicating SUCCESS after threads and tasks were freed.
 */
static int free_pool(threadpool_t* tpp)
{
    for (int thread_idx = 0; thread_idx < tpp->thread_count; thread_idx++)
    {
        free(tpp->threads[thread_idx]);
    }

    free(tpp->threads);

    struct tp_task_s* next_task_p;
    struct tp_task_s* task_head = tpp->task_head;

    while(task_head != NULL)
    {
        next_task_p = task_head->next_task;
        free(task_head);
        task_head = next_task_p;
    }

    free(tpp);

    return TP_SUCCESS;
}

/**
 *  @fn static void* start_thread(threadpool_t* tpp) (function)
 *
 *  This function is called immediately on creation of a new thread. It checks
 *  the threadpool to see if its supposed to shutdown. Then it checks the tasks
 *  list to see if there is any work to perform. If there is no work and the
 *  shutdown flag was not set on the threadpool, this function will wait. If
 *  there is a task in the list, the function will lock the threadpool, claim
 *  the task, unlock the threadpool, and start performing the task.
 *
 *  @param tpp Pointer to the threadpool that created the thread
 */
static void* start_thread(threadpool_t* tpp)
{
    struct tp_task_s* run_task;

    for (;;)
    {
        pthread_mutex_lock(&(tpp->lock));

        while (!(tpp->task_head) && (TP_DOWN != tpp->state))
        {
            pthread_cond_wait(&(tpp->wake_up), &(tpp->lock));
        }

        if (TP_DOWN == tpp->state)
        {
            break;
        }

        run_task = tpp->task_head;
        tpp->task_head = run_task->next_task;
        tpp->task_count--;

        pthread_mutex_unlock(&(tpp->lock));

        (*(run_task->func_ptr))(run_task->argument);

        free(run_task);
    }

    pthread_mutex_unlock(&(tpp->lock));

    return NULL;
}
