/**
 *  @file threadpool.h
 *  @author Ryan Greer <ryan.greer@outlook.com>
 *  @date 22 October 2019
 *
 *  @brief Header file for the Threadpool.
 */

#ifndef THREADPOOL_H
#define THREADPOOL_H

#include <pthread.h>

/** Return codes used by threadpool.c functions. */
enum ThreadpoolCodes
{
    ENULLPTR = -6, /**< Error indicating the function received a NULL pointer argument */
    ETHREADLCK,    /**< Error indicating an error with the threadpool lock */
    TP_FAIL,       /**< Indicates the function has failed to perform */
    TP_SUCCESS,       /**< Indicates the function has completed successfully */
    TP_READY,         /**< Indicates the threadpool is ready */
    TP_DOWN,      /**< Indicates the threadpool has begun shutting down */
};

/** Struct representing an item in the task linked list */
struct tp_task_s {
    void (*func_ptr)(void*);             /**< Function pointer for a thread to execute */
    void* argument;                      /**< Argument to the function                 */
    struct tp_task_s* next_task;         /**< Pointer to the next threadpool task      */
};

/** Reusable type for the threadpool struct */
typedef struct {
    pthread_mutex_t lock; /**< Used to lock the pool */
    pthread_cond_t wake_up;          /**< Send shutdown notification to all threads */
    pthread_t** threads;             /**< Array of pointers to pthreads */
    struct tp_task_s* task_head;     /**< Pointer to the first task in a linked list of tasks */
    int thread_count;                /**< Number of threads in the threadpool */
    int task_count;                  /**< Number of tasks assigned to threadpool */
    int state;                      /**< State of the threadpool */
} threadpool_t;

/**
 *  @fn threadpool_t* threadpool_create(const int thread_count, const int task_limit) (function)
 *
 *  Creates a threadpool with thread_count threads and no more than task_limit
 *  tasks. Returns a pointer to the threadpool. If thread_count is not greater
 *  than 0 or the task_limit is not at least thread_count, NULL is returned and
 *  errno is set to EINVAL. On any other error, NULL is returned and errno
 *  reflects the value set by the function that failed.
 *
 *  @param thread_count Integer indicating the number of worker threads to
 *                      create. Must be a positive integer.
 *  @param task_limit   Integer indicating the maximum number of tasks that can
 *                      be in the task list at any given time. Must be at least
 *                      as large at thread_count.
 *
 *  @return Pointer to a threadpool on success. On error, return NULL.
 */
threadpool_t* threadpool_create(const int thread_count, const int task_limit);

/**
 *  @fn int threadpool_schedule(threadpool_t* tpp, void (*func_ptr)(void*), void* argument) (function)
 *
 *  Run a task using the threadpool. If a thread is idle, it will perform the
 *  task immediately. If there are no idle threads, the task will be scheduled
 *  in the task list of the threadpool. If the task list is full, return
 *  EBUSY. This function does not block when the threadpool task list
 *  is full. It is recommended that the caller do the blocking if desired.
 *
 *  @param tpp Pointer to a threadpool_t used to perform the task
 *  @param func_ptr Void pointer to a function which will be called by the pool
 *  @param args Void pointer to the first argument in an array of arguments
 *  @param num_args Integer indicating the number of arguments in the array
 *
 *  @return Integer SUCCESS indicating the task was successfully scheduled.
 *          Return EBUSY if the threadpool's task list is full. Return ENULLPTR
 *          if the tpp is a NULL pointer.
 */
int threadpool_schedule(threadpool_t* tpp, void (*func_ptr)(void*), void* argument);

/**
 *  @fn int threadpool_shutdown(threadpool_t* tpp) (function)
 *
 *  Sets the threadpool status to THREADPOOL_SHUTDOWN, waits for all threads
 *  to finish their current tasks, joins them all together and frees the
 *  memory.
 *
 *  @param threadpool_p threadpool_t pointer to the threadpool struct
 *
 *  @return Integer indicating SUCCESS if the shutdown process was successful.
 *          Return FAILURE if the threadpool was not shutdown correctly.
 */
int threadpool_shutdown(threadpool_t* tpp);

#endif
