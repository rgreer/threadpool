# threadpool

Custom threadpool library written in C and built as a shared object.

## Table of Contents

- [Dependencies](#dependencies)
- [Installation](#installation)
- [Usage](#usage)
- [Testing](#testing)
- [Documentation](#documentation)

## Dependencies <a name="dependencies"></a>

Project dependencies are broken down into the following categories: run, build, test, and doc. These reflect to the dependencies of the respective make targets.

Run the client and server:

* Ubuntu (written and tested on Ubuntu 19.04)

Build the client and server:

* make
* gcc

Test the threadpool:

* cspec <https://githuh.com/mumuki/cspec>

Compile the project documentation:

* doxygen <http://www.doxygen.nl>
* texlive-latex-base
* texlive-fonts-recommended
* texlive-fonts-extra
* texlive-latex-extra

Verify dependencies by running `./check_deps.sh`.

Running `make setup` will check depenencies and may install additional dependencies.

## Installation <a name="installation"></a>

Install the library using the following commands:
```
make
sudo make install
```

## Usage <a name="usage"></a>

This library may be used to create a threadpool, schedule tasks to be executed by the threadpool, and shutdown the threadpool.

After installing, include `<threadpool.h>` in your source file. This will provide access to the following functions:

* threadpool\_t\* threadpool\_create(const int thread\_count, const int task\_limit)
* int threadpool\_schedule(threadpool\_t\* tpp, void (\*func\_ptr)(void\*), void\* argument)
* int threadpool\_shutdown(threadpool\_t\* tpp)

The library also defines a reusable type representing the threadpool:
* typedef struct threadpool_t;

## Testing <a name="testing"></a>

This project uses the CSpec framework <https://github.com/mumuki/cspec> to perform unit testing. The tests are located under the `tests` directory in the root of the project. Each source file should have a corresponding test_filename.c in the tests directory.

Run the tests using `make test` or use the test-watch <https://gitlab.com/ryan.greer/make-watch> tool to automatically run `make test` while developing.

## Documentation <a name="documentation"></a>

This project uses Doxygen to generate documention from source code and comments. The Doxygen guidelines can be found here: http://www.doxygen.nl

To generate the documentation in HTML format, run the following:
```
make doc
```
This will drop the HTML docs in `doc/html` where it can be accessed in the browser by nagivating to `$project_repo/doc/html/index.html` which will bring you to a copy of this README.md which links to the rest of the documentation.

