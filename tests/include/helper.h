/**
 *  @file helper.h
 *  @author Ryan Greer <ryan.greer@outlook.com>
 *  @date 20 September 2019
 *
 *  @brief Header file for the CSpec test suite helpers.
 */

/**
 *  @fn void context_name(char* name) (function)
 *
 *  This helper function prints a banner with the context name since
 *  CSpec does not.
 *
 *  @param name String representing the cspec context name
 */
void context_name(char* name);

/**
 *  @fn void test_func(void* argument) (function)
 *
 *  This helper function is passed as a function pointer during thread testing.
 *
 */
void test_func(void* argument);
