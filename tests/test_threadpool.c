/**
 *  @file test_threadpool.c
 *  @author Ryan Greer <ryan.greer@outlook.com>
 *  @date 22 October 2019
 *
 *  @brief CSpec tests for Threadpool.
 *
 *  This program tests the net shared library using the cspec framework which
 *  can be be found here:
 *      https://github.com/mumuki/cspec
 */

/* Standard Library Headers */
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/* Dependency Library Headers */
#include <cspecs/cspec.h>

/* Chat Server Headers */
#include "threadpool.h"

/* Test Helper Headers */
#include "helper.h"

/**
 *  Unit tests for the Threadpool.
 */
context(threadpool)
{
    context_name("Threadpool Unit Tests");

    /* Setup for unit_threadpool context */
    threadpool_t* threadpool_p = NULL;
    threadpool_t* single_thread_p = NULL;

    /* Public functions */
    describe("threadpool_create(thread_count, task_limit)") {
        it("Given invalid arguments -> should return NULL") {
            should_ptr(threadpool_create(0, 0)) be null;
            should_ptr(threadpool_create(2, 1)) be null;
        } end

        it("Given valid arguments -> should return pointer to threadpool") {
            should_ptr(threadpool_p = threadpool_create(2, 10)) not be null;
            should_ptr(single_thread_p = threadpool_create(1, 10)) not be null;
        } end

        it("Given a valid threadpool\n"
           "        -> should have thread_count of 2\n"
           "        -> should have a NULL task_head\n"
           "        -> should have task_count of 0") {
            should_int(threadpool_p->thread_count) be equal to(2);
            should_ptr(threadpool_p->task_head) be null;
            should_int(threadpool_p->task_count) be equal to(0);
        } end
    } end

    describe("threadpool_schedule(tpp, func_ptr, argument)") {
        it("Given a NULL argument -> should return ENULLPTR") {
            char* string = "Void Argument";
            should_int(threadpool_schedule(NULL, (void*) &test_func, (void*) string)) be equal to(ENULLPTR);
            should_int(threadpool_schedule(threadpool_p, NULL, (void*) string)) be equal to(ENULLPTR);
            should_int(threadpool_schedule(threadpool_p, (void*) &test_func, NULL)) be equal to(ENULLPTR);
        } end

        it("Given valid arguments -> should return SUCCESS") {
            char* string = "Void Argument";
            should_int(threadpool_schedule(threadpool_p, (void*) &test_func, (void*) string)) be equal to(TP_SUCCESS);
        } end

        it("Given multiple tasks in succession -> should return SUCCESS (for each)") {
            char* string = "Void Argument.";

            for (int i = 0; i < 100; i++)
            {
                threadpool_schedule(single_thread_p, (void*) &test_func, (void*) string);
            }

            should_int(threadpool_schedule(threadpool_p, (void*) &test_func, (void*) string)) be equal to(TP_SUCCESS);
        } end
    } end

    describe("threadpool_shutdown(tpp)")
    {
        it("Given a NULL argument -> should return ENULLPTR") {
            should_int(threadpool_shutdown(NULL)) be equal to(ENULLPTR);
        } end

        it("Given a valid threadpool -> should return SUCCESS") {
            should_int(threadpool_shutdown(threadpool_p)) be equal to(SUCCESS);
            should_int(threadpool_shutdown(single_thread_p)) be equal to(SUCCESS);
        } end
    } end
}
