/**
 *  @file helper.c
 *  @author Ryan Greer <ryan.greer@outlook.com>
 *  @date 22 October 2019
 *
 *  @brief Helper functions for CSpec test suite.
 *
 *  This program tests the net shared library using the cspec framework which
 *  can be be found here:
 *      https://github.com/mumuki/cspec
 */

/** Standard Library Headers */
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/** Set the name of a given context and print out a pretty banner */
void context_name(char* name)
{
    int len = strlen(name);

    char rule[len];
    memset(rule, '=', len);
    rule[len] = '\0';

    printf("\n%s\n", rule);
    puts(name);
    puts(rule);
}

/** Test function used within a thread */
void test_func(void* argument)
{
    (void)(argument);
    sleep(0.5);
}
