CC=gcc
CFLAGS=-g -Wall -Werror -Wextra
GCOV_FLAGS=-ftest-coverage -fprofile-arcs

inc=include
bin=bin
obj=obj
lib=lib
src=src

test_inc=tests/include
test_src=tests

.PHONY: test doc

build: clean
	$(CC) -c -fPIC -o $(obj)/threadpool.o src/threadpool.c $(CFLAGS) -Iinclude/
	$(CC) -shared -Wl,-soname,libthreadpool.so -o $(lib)/libthreadpool.so $(obj)/threadpool.o $(CFLAGS)
	rm -f $(obj)/threadpool.o

install:
	mkdir -p /usr/local/lib
	cp -u $(lib)/libthreadpool.so /usr/local/lib
	mkdir -p /usr/local/include
	cp -u $(inc)/threadpool.h /usr/local/include

uninstall:
	rm -f /usr/local/lib/threadpool.so
	rm -f /usr/local/include/threadpool.h

clean:
	rm -rf $(bin)
	rm -rf $(obj)
	rm -rf $(lib)
	mkdir -p $(bin)
	mkdir -p $(obj)
	mkdir -p $(lib)

test: clean
	cd $(obj) && $(CC) -c ../$(src)/threadpool.c -I../$(inc) -lpthread $(CFLAGS) $(GCOV_FLAGS)
	$(CC) $(obj)/threadpool.o $(test_src)/*.c -I$(inc) -I$(test_inc) -lgcov -lcspecs -lpthread -o $(bin)/test $(CFLAGS)
	valgrind --leak-check=full --show-leak-kinds=all $(bin)/test
	gcovr -p

doc:
	rm -rf doc/html
	rm -rf doc/latex
	doxygen doc/config
	cd doc/latex && echo \r | pdflatex refman.tex >/dev/null ; cp refman.pdf ../README.pdf

all: setup build
